package com.learn.domain.entity

data class Mars(
    val id: String,
    val imgSrcUrl: String,
    val type: String,
    val price: Double
)
