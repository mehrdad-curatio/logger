package com.learn.domain.repository

import com.learn.domain.entity.DevByteVideo
import com.learn.domain.entity.Mars

interface MarsRepository {

    suspend fun getProperties(): List<Mars>
}