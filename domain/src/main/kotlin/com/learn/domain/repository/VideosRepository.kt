package com.learn.domain.repository

import com.learn.domain.entity.DevByteVideo

interface VideosRepository {

    suspend fun getPlaylist(): List<DevByteVideo>
    fun printVideo()
}