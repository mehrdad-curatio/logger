package com.learn.domain.datasources

import com.learn.domain.entity.DevByteVideo
import com.learn.domain.repository.VideosRepository

class VideosDataSource(private val remote:VideosRepository, private val local:VideosRepository?= null): VideosRepository {
    override suspend fun getPlaylist(): List<DevByteVideo> {
        return remote.getPlaylist()
    }

    override fun printVideo() {
        remote.printVideo()
    }
}