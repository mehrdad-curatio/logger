package com.learn.domain.datasources

import com.learn.domain.entity.Mars
import com.learn.domain.repository.MarsRepository

class MarsDataSource(private val remote: MarsRepository, private val local: MarsRepository? = null) : MarsRepository{
    override suspend fun getProperties(): List<Mars> {
        return remote.getProperties()
    }
}