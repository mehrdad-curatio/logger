


buildscript {

    // Versions for all the dependencies we plan to use. It's particularly useful for kotlin and
    // navigation where the versions of the plugin needs to be the same as the version of the
    // library defined in the app Gradle file
    extra.set("versionCore","1.3.1")
    extra.set("versionKotlin","1.4.20")
    extra.set("versionKotlinCoroutines","1.4.2")
    extra.set("versionMoshi","1.11.0")
    extra.set("versionRetrofit","2.9.0")
    extra.set("versionRetrofitCoroutinesAdapter","0.9.2")

    repositories {
        jcenter()
    }
}


allprojects {
    repositories {
        // Use jcenter for resolving dependencies.
        // You can declare any Maven/Ivy/file repository here.
        jcenter()
    }
}

object DependencyVersions {
    const val JETTY_VERSION = "9.4.12.v20180830"
}

