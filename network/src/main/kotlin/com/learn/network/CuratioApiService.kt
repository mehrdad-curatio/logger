package com.learn.network

import com.learn.network.api.mars.MarsApi
import com.learn.network.api.video.VideoApi
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private const val BASE_URL =
    "https://android-kotlin-fun-mars-server.appspot.com"

private const val BASE_URL_NEWS =
    "https://android-kotlin-fun-mars-server.appspot.com/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

object CuratioApiService {
    val marsService : MarsApi by lazy {
        retrofit.create(MarsApi::class.java) }

    val videosService : VideoApi by lazy {
        retrofit.create(VideoApi::class.java) }
}