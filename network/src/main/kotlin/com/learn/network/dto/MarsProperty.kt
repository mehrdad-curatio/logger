package com.learn.network.dto

import com.learn.domain.entity.Mars
import com.squareup.moshi.Json

data class MarsProperty(
    val id: String,
    @Json(name = "img_src") val imgSrcUrl: String,
    val type: String,
    val price: Double
)


fun MarsProperty.asDomainModel() : Mars {
    return Mars(id,imgSrcUrl,type,price)
}

fun MarsProperty.asMars() : Mars {
    return Mars(id,imgSrcUrl,type,price)
}
