package com.learn.network.api.video

import com.learn.domain.entity.DevByteVideo
import com.learn.domain.repository.VideosRepository
import com.learn.network.CuratioApiService
import com.learn.network.dto.asDevByteVideo

class RemoteVideosStorage: VideosRepository {
    override suspend fun getPlaylist(): List<DevByteVideo> {
        return CuratioApiService.videosService.getPlaylist()
            .videos.map { it.asDevByteVideo() }
    }

    override fun printVideo() {
        print("This is message from Video Repo")
    }
}