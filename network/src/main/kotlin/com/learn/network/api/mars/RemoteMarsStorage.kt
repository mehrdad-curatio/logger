package com.learn.network.api.mars

import com.learn.domain.entity.Mars
import com.learn.domain.repository.MarsRepository
import com.learn.network.CuratioApiService
import com.learn.network.dto.asMars

class RemoteMarsStorage : MarsRepository {
    override suspend fun getProperties(): List<Mars> {
        return CuratioApiService.marsService.getProperties()
            .map { it.asMars() }
    }
}