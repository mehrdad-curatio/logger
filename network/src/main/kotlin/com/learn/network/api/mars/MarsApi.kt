package com.learn.network.api.mars

import com.learn.network.dto.MarsProperty
import retrofit2.http.GET

interface MarsApi {

    @GET("realestate")
    suspend fun getProperties(): List<MarsProperty>
}