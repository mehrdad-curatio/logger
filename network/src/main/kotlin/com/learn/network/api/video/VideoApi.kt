package com.learn.network.api.video

import com.learn.network.dto.NetworkVideoContainer
import retrofit2.http.GET

interface VideoApi {
    @GET("devbytes")
    suspend fun getPlaylist(): NetworkVideoContainer
}