package com.learn.domain.di

import com.learn.domain.datasources.MarsDataSource
import com.learn.domain.datasources.VideosDataSource
import com.learn.domain.repository.MarsRepository
import com.learn.domain.repository.VideosRepository
import com.learn.network.api.mars.RemoteMarsStorage
import com.learn.network.api.video.RemoteVideosStorage

class DomainFactory {

    // marsStorage creation
    private val marsRemoteStorage:RemoteMarsStorage by lazy {
         RemoteMarsStorage()
    }

    val marsRepository:MarsRepository by lazy {
        MarsDataSource(marsRemoteStorage)
    }

    // Video Repository creation
    private val videoRemoteStorage: RemoteVideosStorage by lazy {
        RemoteVideosStorage()
    }

    val videoRepository: VideosRepository by lazy {
        VideosDataSource(videoRemoteStorage)
    }



}